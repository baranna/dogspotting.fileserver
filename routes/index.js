const {MLServerBaseUrl} =  require("../config");

const { v4: uuidv4 } = require('uuid');
var express = require('express');
var router = express.Router();
var path = require('path');
var axios = require('axios');

router.post('/upload', function (req, res, next) {
    if (!req.files || Object.keys(req.files).length === 0) {
        return res.status(400).send('No files were uploaded.');
    }

    let dogPicture = req.files.dogPicture;
    let fileExtensionRegexp = /\.[0-9a-z]+$/i;
    let match = fileExtensionRegexp.exec(dogPicture.name);
    let url = `images/dogs/${uuidv4()}${match[0]}`

    dogPicture.mv(path.join(__dirname, '..', 'public', url), function (err) {
        if (err)
            return res.status(500).send(err);

        axios.post(MLServerBaseUrl + "detect-dog", {"url": "http://localhost:3001/" + url}).then(
            result => res.json({"url": url, dogDetected: result.data.result })
        )

    });
});

module.exports = router;
